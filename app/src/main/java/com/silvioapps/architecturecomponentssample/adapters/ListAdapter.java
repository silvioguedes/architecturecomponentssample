package com.silvioapps.architecturecomponentssample.adapters;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.silvioapps.architecturecomponentssample.R;
import com.silvioapps.architecturecomponentssample.models.DateTime;
import java.util.List;

import io.reactivex.Flowable;

public class ListAdapter extends PagedListAdapter<DateTime, ListAdapter.ViewHolder> {
    private List<DateTime> list = null;

    public ListAdapter() {
        super(DIFF_CALLBACK);
    }

    public void setData(List<DateTime> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.text_view_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        if(getItemCount() > 0) {
            DateTime obj = list.get(i);
            if(obj != null) {
                viewHolder.bindTo(obj);
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView = null;

        public ViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.textView);
        }

        public void bindTo(DateTime dateTime) {
            textView.setText(dateTime.getTime() + " " + dateTime.getDate());
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static final DiffCallback<DateTime> DIFF_CALLBACK = new DiffCallback<DateTime>() {
        @Override
        public boolean areItemsTheSame(@NonNull DateTime oldObj, @NonNull DateTime newObj) {
            // User properties may have changed if reloaded from the DB, but ID is fixed
            return oldObj.getId() == newObj.getId();
        }
        @Override
        public boolean areContentsTheSame(@NonNull DateTime oldObj, @NonNull DateTime newObj) {
            // NOTE: if you use equals, your object must properly override Object#equals()
            // Incorrectly returning false here will result in too many animations.
            return oldObj.equals(newObj);
        }
    };
}
