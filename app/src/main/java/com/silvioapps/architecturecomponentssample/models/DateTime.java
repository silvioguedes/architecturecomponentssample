package com.silvioapps.architecturecomponentssample.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

@Entity(tableName = "DateTime")
public class DateTime{
	@PrimaryKey(autoGenerate = true)//se o banco não possuir id's diferentes, o retorno sempre será um único item
	int id;

	@ColumnInfo(name = "date")
	private String date;

	@ColumnInfo(name = "milliseconds_since_epoch")
	private long millisecondsSinceEpoch;

	@ColumnInfo(name = "time")
	private String time;

    public interface API {
        @Headers({"Accept: application/json"})
        @GET("/")
		Call<DateTime> get();
    }

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setMillisecondsSinceEpoch(long millisecondsSinceEpoch){
		this.millisecondsSinceEpoch = millisecondsSinceEpoch;
	}

	public long getMillisecondsSinceEpoch(){
		return millisecondsSinceEpoch;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}