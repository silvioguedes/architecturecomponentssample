package com.silvioapps.architecturecomponentssample.database.daos;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListProvider;
import android.arch.paging.TiledDataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.silvioapps.architecturecomponentssample.models.DateTime;
import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface IDateTimeDao {
    @Query("SELECT * FROM DateTime")
    LiveData<List<DateTime>> getAll();

    @Query("SELECT * FROM DateTime WHERE date = :date")
    LivePagedListProvider<Integer, DateTime> getTimeFromDate(String date);

    @Query("SELECT * FROM DateTime")
    Flowable<List<DateTime>> getFlowable();

    @Insert
    void insert(DateTime obj);
}