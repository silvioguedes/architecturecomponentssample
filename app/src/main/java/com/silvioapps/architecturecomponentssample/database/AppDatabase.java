package com.silvioapps.architecturecomponentssample.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import com.silvioapps.architecturecomponentssample.database.daos.IDateTimeDao;
import com.silvioapps.architecturecomponentssample.models.DateTime;

@Database(entities = {DateTime.class}, version = 5)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance = null;

    public static AppDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context, AppDatabase.class, "database.db").build();
        }

        return instance;
    }

    public static void destroyInstance(){
        instance = null;
    }

    public abstract IDateTimeDao dateTimeDao();

    public static class InsertDateTimeAsyncTask extends AsyncTask<DateTime, Void, Void>
    {
        @Override
        protected void onPreExecute (){}

        @Override
        protected Void doInBackground(DateTime...arg) {
            if(instance != null) {
                instance.dateTimeDao().insert(arg[0]);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void arg) {}
    }
}
