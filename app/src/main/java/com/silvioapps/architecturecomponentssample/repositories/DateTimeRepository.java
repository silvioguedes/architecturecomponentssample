package com.silvioapps.architecturecomponentssample.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.paging.PagedList;
import android.arch.paging.TiledDataSource;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;
import com.silvioapps.architecturecomponentssample.architecture.NetworkBoundResource;
import com.silvioapps.architecturecomponentssample.architecture.RateLimiter;
import com.silvioapps.architecturecomponentssample.architecture.Resource;
import com.silvioapps.architecturecomponentssample.database.daos.IDateTimeDao;
import com.silvioapps.architecturecomponentssample.models.DateTime;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;

public class DateTimeRepository {
    private Context context = null;
    private DateTime.API api = null;
    private IDateTimeDao dateTimeDao = null;
    private RateLimiter<String> rateLimiter = new RateLimiter<>(10, TimeUnit.SECONDS);
    private LiveData<PagedList<DateTime>> pagedListLiveData = null;
    private LiveData<List<DateTime>> listLiveData = null;
    //limite máximo de tempo para buscar os dados remotamente é de 10 segundos
    private static final int INITIAL_LOAD_POSITION = 0;
    private static final int PAGE_SIZE = 5;
    private static final int PREFETCH_DISTANCE = 5;

    @Inject
    public DateTimeRepository(Context context, DateTime.API api, IDateTimeDao dateTimeDao){
        this.context = context;
        this.api = api;
        this.dateTimeDao = dateTimeDao;
    }

    public LiveData<Resource<List<DateTime>>> getLiveDataResourceList() {
        return new NetworkBoundResource<List<DateTime>, DateTime>() {

            @Override
            protected void saveCallResult(@NonNull DateTime item) {
                dateTimeDao.insert(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<DateTime> data) {
                return data == null || data.isEmpty() || rateLimiter.shouldFetch();
            }

            @NonNull
            @Override
            protected LiveData<List<DateTime>> loadFromDb() {
                return dateTimeDao.getAll();
            }

            @NonNull
            @Override
            protected Call<DateTime> createCall() {
                return api.get();
            }

            @Override
            protected void onFetchFailed() {
                Toast.makeText(context, "Fetch failed!", Toast.LENGTH_SHORT).show();
            }

        }.getAsLiveData();
    }

    public LiveData<Resource<PagedList<DateTime>>> getPagedListLiveData(final String date){
        return new NetworkBoundResource<PagedList<DateTime>, DateTime>() {

            @Override
            protected void saveCallResult(@NonNull DateTime item) {
                dateTimeDao.insert(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable PagedList<DateTime> data) {
                return data == null || data.isEmpty() || rateLimiter.shouldFetch();
            }

            @NonNull
            @Override
            protected LiveData<PagedList<DateTime>> loadFromDb() {
                if(pagedListLiveData == null){
                    pagedListLiveData = dateTimeDao.getTimeFromDate(date).create(
                            INITIAL_LOAD_POSITION,
                            new PagedList.Config.Builder()
                                    .setPageSize(PAGE_SIZE)
                                    .setPrefetchDistance(PREFETCH_DISTANCE)
                                    .build());
                }

                return pagedListLiveData;
            }

            @NonNull
            @Override
            protected Call<DateTime> createCall() {
                return api.get();
            }

            @Override
            protected void onFetchFailed() {
                Toast.makeText(context, "Fetch failed!", Toast.LENGTH_SHORT).show();
            }

        }.getAsLiveData();
    }

    public LiveData<Resource<List<DateTime>>> getLiveDataResourceListFromFlowable() {
        return new NetworkBoundResource<List<DateTime>, DateTime>() {

            @Override
            protected void saveCallResult(@NonNull DateTime item) {
                dateTimeDao.insert(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<DateTime> data) {
                return data == null || data.isEmpty() || rateLimiter.shouldFetch();
            }

            @NonNull
            @Override
            protected LiveData<List<DateTime>> loadFromDb() {
                if(listLiveData == null) {
                    listLiveData = LiveDataReactiveStreams.
                            fromPublisher(dateTimeDao.getFlowable().
                            subscribeOn(Schedulers.newThread()).
                            observeOn(AndroidSchedulers.mainThread()));
                }

                return listLiveData;
            }

            @NonNull
            @Override
            protected Call<DateTime> createCall() {
                return api.get();
            }

            @Override
            protected void onFetchFailed() {
                Toast.makeText(context, "Fetch failed!", Toast.LENGTH_SHORT).show();
            }

        }.getAsLiveData();
    }
}