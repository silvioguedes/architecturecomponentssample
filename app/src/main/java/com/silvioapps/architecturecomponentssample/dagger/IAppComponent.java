package com.silvioapps.architecturecomponentssample.dagger;

import android.app.Application;
import com.silvioapps.architecturecomponentssample.application.CustomApplication;
import com.silvioapps.architecturecomponentssample.modules.AppModule;
import javax.inject.Singleton;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by silvio on 19/08/17.
 */

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class})
public interface IAppComponent{

    @Component.Builder
    interface Builder{
        @BindsInstance Builder application(Application application);
        IAppComponent build();
    }

    void inject(CustomApplication application);
}
