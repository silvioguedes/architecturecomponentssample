package com.silvioapps.architecturecomponentssample.modules;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import com.silvioapps.architecturecomponentssample.view_models.DateTimeViewModel;
import com.silvioapps.architecturecomponentssample.view_models.utils.ViewModelFactory;
import com.silvioapps.architecturecomponentssample.view_models.utils.ViewModelKey;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelsModule {
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(DateTimeViewModel.class)
    abstract ViewModel bindDateTimeViewModel(DateTimeViewModel viewModel);
}