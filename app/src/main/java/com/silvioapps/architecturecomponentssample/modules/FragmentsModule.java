package com.silvioapps.architecturecomponentssample.modules;

import com.silvioapps.architecturecomponentssample.fragments.MainFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by silvio on 19/08/17.
 */

@Module
public abstract class FragmentsModule {
    @ContributesAndroidInjector
    abstract MainFragment contributesMainFragment();
}
