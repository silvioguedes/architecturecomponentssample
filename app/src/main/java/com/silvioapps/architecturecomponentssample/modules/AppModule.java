package com.silvioapps.architecturecomponentssample.modules;

import android.app.Application;
import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.silvioapps.architecturecomponentssample.database.AppDatabase;
import com.silvioapps.architecturecomponentssample.database.daos.IDateTimeDao;
import com.silvioapps.architecturecomponentssample.models.DateTime;
import java.io.IOException;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by silvio on 19/08/17.
 */

@Module(includes = {ActivitiesModule.class, FragmentsModule.class, ViewModelsModule.class})
public class AppModule{

    @Provides
    @Singleton
    public Context providesContext(Application application){
        return application;
    }

    @Provides
    @Singleton
    public DateTime.API providesAPI(){//provê a instância da API
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });
        OkHttpClient client = httpClient.build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl("http://date.jsontest.com")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(DateTime.API.class);
    }

    @Provides
    @Singleton//provê a instância do DAO de DateTime através da instância de Context
    public IDateTimeDao providesIDateTimeDao(Context context){
        return AppDatabase.getInstance(context).dateTimeDao();
    }
}

