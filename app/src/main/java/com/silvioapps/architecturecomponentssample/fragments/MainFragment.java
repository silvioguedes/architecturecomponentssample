package com.silvioapps.architecturecomponentssample.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.silvioapps.architecturecomponentssample.R;
import com.silvioapps.architecturecomponentssample.adapters.ListAdapter;
import com.silvioapps.architecturecomponentssample.architecture.Resource;
import com.silvioapps.architecturecomponentssample.architecture.Status;
import com.silvioapps.architecturecomponentssample.models.DateTime;
import com.silvioapps.architecturecomponentssample.view_models.DateTimeViewModel;
import java.util.List;
import javax.inject.Inject;
import dagger.android.support.AndroidSupportInjection;

//não é mais necessário que o fragmento seja do tipo LifecycleFragment
public class MainFragment extends Fragment{
    private ListAdapter listAdapter = null;
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public void onAttach(Context context){
        AndroidSupportInjection.inject(this);

        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        View view = layoutInflater.inflate(R.layout.fragment_main, viewGroup, false);

        listAdapter = new ListAdapter();

        if(getActivity() != null) {
            RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(listAdapter);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        DateTimeViewModel dateTimeViewModel = ViewModelProviders.of(this, viewModelFactory).get(DateTimeViewModel.class);

        //todo use para inserir dados sem manipulações ou paginação
        /*dateTimeViewModel.getLiveDataResourceList().observe(this, new Observer<Resource<List<DateTime>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<DateTime>> dateTimeResource) {
                if(dateTimeResource != null) {
                    if (dateTimeResource.status == Status.SUCCESS) {
                        listAdapter.setData(dateTimeResource.data);
                    }
                    else if (dateTimeResource.status == Status.ERROR) {
                        //todo show error
                    }
                    else if (dateTimeResource.status == Status.LOADING) {
                        //todo show loading
                    }
                }
            }
        });*/

        //todo use para testar manipulações de livedata no repositório
        /*dateTimeViewModel.getLiveDataSwitchedResourceDateTime().observe(this, new Observer<Resource<List<DateTime>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<DateTime>> dateTimeResource) {
                if(dateTimeResource != null) {
                    if (dateTimeResource.status == Status.SUCCESS) {
                        listAdapter.setData(dateTimeResource.data);
                    }
                    else if (dateTimeResource.status == Status.ERROR) {
                        //todo show error
                    }
                    else if (dateTimeResource.status == Status.LOADING) {
                        //todo show loading
                    }
                }
            }
        });*/

        //todo use para paginar o banco de dados e somente retornar 5 ocorrências de uma data específica
        /*dateTimeViewModel.getPagedListLiveData("09-25-2017").observe(this, new Observer<Resource<PagedList<DateTime>>>() {
            @Override
            public void onChanged(@Nullable Resource<PagedList<DateTime>> dateTimeResource) {
                if (dateTimeResource != null) {
                    if (dateTimeResource.status == Status.SUCCESS) {
                        listAdapter.setData(dateTimeResource.data);
                    } else if (dateTimeResource.status == Status.ERROR) {
                        //todo show error
                    } else if (dateTimeResource.status == Status.LOADING) {
                        //todo show loading
                    }
                }
            }
        });*/

        //todo use para testar o RxJava no Room
        dateTimeViewModel.getFlowableListLiveData().observe(this, new Observer<Resource<List<DateTime>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<DateTime>> dateTimeResource) {
                if (dateTimeResource != null) {
                    if (dateTimeResource.status == Status.SUCCESS) {
                        listAdapter.setData(dateTimeResource.data);
                    } else if (dateTimeResource.status == Status.ERROR) {
                        //todo show error
                    } else if (dateTimeResource.status == Status.LOADING) {
                        //todo show loading
                    }
                }
            }
        });
    }
}
