package com.silvioapps.architecturecomponentssample.architecture;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}