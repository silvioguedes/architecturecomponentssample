package com.silvioapps.architecturecomponentssample.view_models;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;
import com.silvioapps.architecturecomponentssample.architecture.Resource;
import com.silvioapps.architecturecomponentssample.models.DateTime;
import com.silvioapps.architecturecomponentssample.repositories.DateTimeRepository;
import java.util.List;
import javax.inject.Inject;

public class DateTimeViewModel extends ViewModel {
    private DateTimeRepository dateTimeRepository = null;

    @Inject
    public DateTimeViewModel(DateTimeRepository dateTimeRepository){
        this.dateTimeRepository = dateTimeRepository;
    }

    //usando a classe Transformations para mudar a data nos objetos DateTime para 01/01/0001
    public LiveData<Resource<List<DateTime>>> getLiveDataResourceDateTime() {
        //map retorna objetos modificados e propaga-os no LiveData (mais demorado)
        return Transformations.map(dateTimeRepository.getLiveDataResourceList(), new Function<Resource<List<DateTime>>, Resource<List<DateTime>>>() {

            @Override
            public Resource<List<DateTime>> apply(Resource<List<DateTime>> input) {
                List<DateTime> list = input.data;
                if(list != null) {
                    for(DateTime obj : list){
                        obj.setDate("01-01-0001");
                    }
                }

                return input;
            }
        });
    }

    public LiveData<Resource<List<DateTime>>> getLiveDataSwitchedResourceDateTime() {
        //switchMap() retorna objetos modificados como LiveData diretamente (mais rápido)
        return Transformations.switchMap(dateTimeRepository.getLiveDataResourceList(), new Function<Resource<List<DateTime>>, LiveData<Resource<List<DateTime>>>>(){

            @Override
            public LiveData<Resource<List<DateTime>>> apply(Resource<List<DateTime>> input) {
                MutableLiveData<Resource<List<DateTime>>> result = new MutableLiveData<>();

                List<DateTime> list = input.data;
                if(list != null) {
                    for (DateTime obj : list) {
                        obj.setDate("01-01-0001");
                    }
                }

                result.setValue(input);

                return result;
            }
        });
    }

    public LiveData<Resource<List<DateTime>>> getLiveDataResourceList(){
        return dateTimeRepository.getLiveDataResourceList();
    }

    public LiveData<Resource<PagedList<DateTime>>> getPagedListLiveData(String date){
        return dateTimeRepository.getPagedListLiveData(date);
    }

    public LiveData<Resource<List<DateTime>>> getFlowableListLiveData(){
        return dateTimeRepository.getLiveDataResourceListFromFlowable();
    }
}